*** Change Logs ***

= 2.1 2021-03-19 =
* Add - Demo file included.
* Fix - Update database link shown in newly created configurator.
* Add - Text string setting added `Build Summary Title`.
* Add - Text string setting added `Get a Quote Form Title`.
* Add - Text string setting added `Get a Quote Button Text`.
* Add - Text string setting added `Get a Quote Submit Button Text`.
* Add - Text string setting added `Name Placeholder`.
* Add - Text string setting added `Email Placeholder`.
* Add - Text string setting added `Phone Placeholder`.
* Add - Text string setting added `Message Placeholder`.
* Localization - Text domain string updated.

= 2.0 2021-03-15 =
* Add - New skin Accordion Style 2.
* Add - New skin Popover.
* Add - Get a quote form support.
* Add - Get a quote mail save as post in `Config Mail List` section.
* Add - Contact form 7 support.
* Add - Switch view functionality.
* Add - Deselect Child functionality( Deselect everything and Deselect siblings ).
* Add - Customize email template.
* Add - Edit configuration after added into the cart.
* Add - View details configuration in cart page.
* Add - Configuration image added in checkout and order details page.
* Add - `Configure It` button added in single product.
* Add - Custom import and export.
* Add - Duplicate configurator.
* Add - Edit and sort view functionality.
* Add - Design options added.
* Enhancement - Detailed price summary.
* Enhancement - Product templates.
* Enhancement - View indicator dot like tab menu.
* Dev - Introduced `wpc_after_reconfigure_link_html` hook.
* Dev - Introduced `wpc_after_preview_inner_html` hook.
* Dev - Introduced `wpc_after_view` hook.
* Dev - Introduced `wpc_after_subset` hook.
* Dev - Introduced `wpc_before_add_to_cart_button` hook.
* Dev - Introduced `wpc_after_add_to_cart_button` hook.
* Dev - Introduced `wpc_after_summary_total_price` hook.
* Dev - Introduced `wpc_after_view_summary_button` hook.
* Dev - Introduced `wpc_before_quote_form_field` hook.
* Dev - Introduced `wpc_quote_form_field_before_message` hook.
* Dev - Introduced `wpc_after_quote_form_field` hook.
* Dev - Introduced `wpc_before_quote_form_submit_button` hook.
* Dev - Introduced `wpc_after_get_quote_trigger_button` hook.
* Dev - Introduced `wpc_after_controls_html` hook.
* Dev - Introduced `wpc_after_total_price` hook.
* Dev - Introduced `wpc_before_share_icons` hook.
* Dev - Introduced `wpc_after_share_icons` hook.
* Dev - Introduced `wpc_after_floating_icons` hook.
* Dev - Introduced `wpc_after_floating_icons_wrapper` hook.
* Dev - Introduced `wpc_control_inner_html` hook.
* Dev - Introduced `wpc_allow_config_image_on_checkout` filter.
* Dev - Introduced `wpc_allow_config_image_on_cart` filter.
* Dev - Introduced `wpc_allow_config_image_on_order_details` filter.
* Dev - Introduced `wpc_save_user_customisation_post_content` filter.
* Dev - Introduced `wpc_quote_form_mail_body` filter.
* Dev - Introduced `wpc_save_user_customisation_args` filter.
* Dev - Introduced `wpc_wpcf7_form_fields` filter.
* Dev - Introduced `wpc_preview_slider_data` filter.
* Dev - Introduced `wpc_allow_phone_field_on_quote_form` filter.
* Dev - Introduced `wpc_inspiration_slider_data` filter.
* Dev - Introduced `wpc_control_data` filter.
* Dev - Introduced `wpc_components_filter` filter.
* Dev - Introduced `wpc_before_save_photo` JS hook.
* Dev - Introduced `wpc_data_changeimage` JS hook.
* Dev - Introduced `wpc_preview_slide_initialized` JS hook.
* Dev - Introduced `wpc_preview_slide_changed` JS hook.
* Dev - Introduced `wpc_preview_slide_resize` JS hook.
* Dev - Introduced `wpc_preview_slide_resized` JS hook.
* Dev - Introduced `wpc_hotspot_clicked-accordion` JS hook.
* Dev - Introduced `wpc_hotspot_clicked-popover` JS hook.
* Dev - Introduced `wpc_after_load_preview` JS hook.
* Dev - Introduced `wpc_inspiration_created` JS hook.
* Dev - Introduced `wpc_inspiration_updated` JS hook.
* Dev - Introduced `wpc_inspiration_resetted` JS hook.
* Dev - Introduced `wpc_inspiration_deleted` JS hook.
* Dev - Introduced `wpc_inspiration_group_deleted` JS hook.


